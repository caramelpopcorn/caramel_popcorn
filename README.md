## Popcorn project - version _caramel_

### Introduction

Caramel is a simple investment tracker for Indian markets
written in vanilla js

### How to Run

1. Install git from [here](https://git-scm.com/).
2. You might have to set your global git settings to match the Atlassian Email.

```
$ git config --global user.name "John Doe"
$ git config --global user.email johndoe@example.com
```

3. Clone the project with `git clone https://VeganRhino@bitbucket.org/caramelpopcorn/caramel_popcorn.git`.

4. Install Node Js from [here](https://nodejs.org/en/).

5. After clone, on terminal, cd to the project base folder.

6. Run `npm install --verbose`. This will download all the relevant dependencies.

7. Once above step is done, run `npm run build` to compile the project.

8. You should be able to serve the pages using `npm run dev-client`.

###### Note: Currenly Only Development Setup is enabled.