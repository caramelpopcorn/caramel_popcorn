### Basics
- We will use Tim Brown's [Modular Type Sizes] (https://alistapart.com/article/more-meaningful-typography/)
    - Start with a good body text size (say 16 px)
    - Identify one good caption text size (say 140 px)
    - Use a ratio like golden ratio to scale up 16 - and scale down 140 - to get list of sizes
    - Stick to these sizes for everything, from font size to margins to measure to spacing 
- The number of characters per line of text is called _Measure_ and we will stick to a measure of 65-70
- Responsive design should ensure that the _Measure_ scales by device width
    - 65-70 for laptops, desktops and bigger screens
    - 50-55 for tablets or phones in landscape mode
    - 35-40 for phones in portrait mode

### Font Choices
- We will use a Sans-Serif font for the Headings and Captions
    - `font-family: 'Open Sans Condensed', sans-serif;`
- We will use a Serif font for body text
    - `font-family: 'Josefin Slab', serif;`
- We can get these fonts from [google fonts] (https://fonts.google.com)
    - ` <link href="https://fonts.googleapis.com/css?family=Josefin+Slab|Open+Sans+Condensed:300&display=swap" rel="stylesheet">`
    - ```<style>```
      ```@import url('https://fonts.googleapis.com/css?family=Josefin+Slab|Open+Sans+Condensed:300&display=swap');```
      ```</style>``` 
